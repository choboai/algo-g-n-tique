# Algo Génétique #

### Informations ###

* Algo Génétique
* Version 1.0

### Comment lancer le programme ###

* Installer python 3.x.x ([https://www.python.org/downloads/](Link URL))
* cloner le projet dans un dossier de votre choix
* Ouvrez Algo Genetique.py avec idle ou autre
* Lancer l'exécution (f5 pour idle)
* ...
* Profit!

### Screenshot ###

![algogen.JPG](https://bitbucket.org/repo/ed788E/images/2668481932-algogen.JPG)

### Me contacter ###

* **auteur :** opmvpc

### source ###

Comment fonctionne l'algorithme([http://www.ai-junkie.com/ga/intro/fr-gat1.html](Link URL))