import random
from random import randrange
from collections import deque

# 1) Creation d'une population
# 2) Calcul du fitness score
# 3) Roulette wheel
# 4) Reproduction + Mutation
# 5) Solutions

#### Variables ####

resultat_a_trouver = 23
taille_population = 10 #doit etre paire
population = []
taille_chromosome = 9 #min 3
chromosome = []
fitness_scores = []
#max_crossover_rate = 0.1
crossover_rate = 0.7
#max_mutation_rate = 1
mutation_rate = 0.001
solutions = []
generations = 1000
input_generations = 1000
cpt_generations = 0
seeder = "sus"
quitter = ""

#genes

taille_genes = 4

genes_nombres = []
genes_nombres.append((0, [0, 0, 0, 0]))
genes_nombres.append((1, [0, 0, 0, 1]))
genes_nombres.append((2, [0, 0, 1, 0]))
genes_nombres.append((3, [0, 0, 1, 1]))
genes_nombres.append((4, [0, 1, 0, 0]))
genes_nombres.append((5, [0, 1, 0, 1]))
genes_nombres.append((6, [0, 1, 1, 0]))
genes_nombres.append((7, [0, 1, 1, 1]))
genes_nombres.append((8, [1, 0, 0, 0]))
genes_nombres.append((9, [1, 0, 0, 1]))

genes_operateurs = []
genes_operateurs.append(("+", [1, 0, 1, 0]))
genes_operateurs.append(("-", [1, 0, 1, 1]))
genes_operateurs.append(("*", [1, 1, 0, 0]))
genes_operateurs.append(("/", [1, 1, 0, 1]))

#genes_inutilises = [[1, 1, 1, 0], [1, 1, 1, 1]]

#### Fonctions ####

def creation_chromosome(nbr_genes, taille):
    un_chromosome = []

    while nbr_genes:
        x = taille
        un_gene = []
        while x:
            un_bit = randrange(0, 2)
            un_gene.append(un_bit)
            x -= 1
        un_chromosome.append(un_gene)
        nbr_genes -= 1

    return un_chromosome

def creation_population(nbr_population):
    une_population = []

    while nbr_population:
        une_population.append(creation_chromosome(taille_chromosome, taille_genes))
        nbr_population -= 1

    return une_population

def calcul_fitness_scores(une_population):
    des_scores = []

    #calcul du total de chaque chromosome
    for un_chromosome in une_population:
        total = 0
        i = 0
        temoin_premier_chiffre = False
        
        #premier chiffre        
        while temoin_premier_chiffre == False and i < len(un_chromosome):
            un_gene = un_chromosome[i]
            for un_gene_nombre in genes_nombres:
                #unpacking du tuple pour comparaison
                [valeur, liste_binaire] = un_gene_nombre
                if un_gene == liste_binaire:
                    premier_chiffre = valeur
                    temoin_premier_chiffre = True
            if temoin_premier_chiffre == False:
                i += 1
        if temoin_premier_chiffre == True:
            total += premier_chiffre
            i +=1
        else:
            total = 0
        #print("\npremier chiffre :", premier_chiffre)
        #print("i premier chiffre =", i-1)
        
        #Recherche des operations suivantes       
        while i < len(un_chromosome) and temoin_premier_chiffre == True:
            temoin_operateur = False
            temoin_chiffre = False
            operateur = ""
            chiffre = 0
            
            #recherche d'un operateur
            while temoin_operateur == False and i < len(un_chromosome):
                un_gene = un_chromosome[i]
                for un_gene_operateur in genes_operateurs:
                    #unpacking du tuple pour comparaison
                    [valeur, liste_binaire] = un_gene_operateur
                    if un_gene == liste_binaire:
                        operateur = valeur
                        temoin_operateur = True
                i += 1
            #print("operateur :", operateur)
            #print("i operateur =", i-1)
            
            #recherche d'un chiffre
            while temoin_chiffre == False and i < len(un_chromosome):
                un_gene = un_chromosome[i]
                for un_gene_nombre in genes_nombres:
                    #unpacking du tuple pour comparaison
                    [valeur, liste_binaire] = un_gene_nombre
                    if un_gene == liste_binaire:
                        chiffre = valeur
                        temoin_chiffre = True
                i += 1
            #print("chiffre :", chiffre)
            #print("i chiffre =", i-1)

            #operation sur le total
            if temoin_chiffre == True and temoin_operateur == True:
                if operateur == "+":
                    total += chiffre
                elif operateur == "-":
                    total -= chiffre
                elif operateur == "*":
                    total *= chiffre
                elif operateur == "/":
                    if chiffre == 0: #eviter division par zero
                        total /= 1
                    else:
                        total /= chiffre
            #print(operateur, chiffre)
            #print("total :", total)
                    
        #calcul du score de chaque chromosome
        if total == resultat_a_trouver: #eviter division par zero
            solutions.append((un_chromosome, cpt_generations)) #enregistrement la solution
            un_score = 1 #valeur a donner?
        else:
            un_score = 1/(total - resultat_a_trouver)
            if un_score < 0: #valeur absolue du score
                un_score *= -1
        des_scores.append(un_score)
        
    return des_scores

def roulette_wheel():
    total_scores = 0
    roulette = []

    #calcul des proportions pour la roulette
    for score in fitness_scores:
        total_scores += score
    #total des scores = 100% de la surface de la roulette
    rapport = 100/total_scores
    total_roulette = 0
    for score in fitness_scores:
        proportion = score * rapport
        total_roulette += proportion
        roulette.append(total_roulette)
    roulette.pop() #dernier element pas toujours = 100
    roulette.append(100)
    #print(roulette)

    #selection aleatoire d'un chromosome
    i = 0
    nombre_aleatoire = randrange(0, 101)
    #print(nombre_aleatoire)
    for une_proportion in roulette:
        if nombre_aleatoire <= une_proportion:
            return i
        i += 1

def reproduction(i_ch1, i_ch2):
    ch1 = population[i_ch1]
    ch2 = population[i_ch2]
    chromosomes = [ch1, ch2]
    taille_vecteur = taille_genes * taille_chromosome
    nouveaux_chromosomes = []
    v_temp = []

    for chromosome in chromosomes:
        vecteur_chromosome = []
        for gene in chromosome:
            for bit in gene:
                vecteur_chromosome.append(bit)
        #print(vecteur_chromosome)
        nouveaux_chromosomes.append(vecteur_chromosome)
    
    #print("\n - Crossover rate =", crossover_rate)
    #reproduction des deux chromosomes?
    chance = crossover_rate * (1 / crossover_rate)
    #print(" - Chance =",chance)
    life_miracle = randrange(1, chance + 1)
    if life_miracle == chance :
        i_coupure = randrange(0, taille_vecteur)
        #print(" - Youpi! i_coupure =", i_coupure)
        v_temp.append(nouveaux_chromosomes[0].copy())
        v_temp.append(nouveaux_chromosomes[1].copy())
        v_temp.append(nouveaux_chromosomes[0].copy())
        v_temp.append(nouveaux_chromosomes[1].copy())
        #print(v_temp[0], "\n",v_temp[1], "\n",v_temp[2], "\n",v_temp[3])

        cpt = i_coupure - taille_vecteur
        if cpt < 0:
            cpt *= -1
        #print("cpt = ", cpt)
        while cpt:
            v_temp[0].pop()
            v_temp[1].pop()
            cpt -= 1

        cpt_2 = i_coupure
        #print("cpt_2 = ", cpt_2)
        v_temp[2] = deque(v_temp[2])
        v_temp[3] = deque(v_temp[3])
        while cpt_2:
            v_temp[2].popleft()
            v_temp[3].popleft()
            cpt_2 -= 1

        v_temp[2] = list(v_temp[2])
        v_temp[3] = list(v_temp[3])
        #print(v_temp[0], "\n",v_temp[1], "\n",v_temp[2], "\n",v_temp[3])
        
        cpt_3 = i_coupure - taille_vecteur
        if cpt_3 < 0:
            cpt_3 *= -1
        #print("cpt_3 = ", cpt_3)
        while cpt_3:
            for un_bit in v_temp[2]:
                v_temp[1].append(un_bit)
                cpt_3 -= 1
                    
        cpt_4 = i_coupure - taille_vecteur
        if cpt_4 < 0:
            cpt_4 *= -1
        #print("cpt_4 = ", cpt_4)
        while cpt_4:
            for un_bit in v_temp[3]:
                v_temp[0].append(un_bit)
                cpt_4 -= 1
                            
        #print(v_temp[0], "\n",v_temp[1], "\n",v_temp[2], "\n",v_temp[3])
        
        nouveaux_chromosomes[0] = v_temp[0]
        nouveaux_chromosomes[1] = v_temp[1]
        #print("\n", nouveaux_chromosomes[0], "\n", nouveaux_chromosomes[1])
    #else:
        #print(" - Pas de reproduction :(") 

    #print("\nMutation :\n")
    chromosomes_finaux = []
    chromosomes_finaux.append(mutation(nouveaux_chromosomes[0]))
    chromosomes_finaux.append(mutation(nouveaux_chromosomes[1]))
    #print("\n", chromosomes_finaux[0], "\n", chromosomes_finaux[1])

    return chromosomes_finaux
        
def mutation(vecteur_chromosome):
    taille_vecteur = taille_genes * taille_chromosome
    chance = 1 * (1 / mutation_rate)
    v_temp = []
    v_temp_2 = []
    
    #print(" - Chance =",chance)
    for bit in vecteur_chromosome:
        life_miracle = randrange(1, chance + 1)
        if life_miracle == chance :
            if bit == 0:
                v_temp.append(1)
            else:
                v_temp.append(0)
        else:
            v_temp.append(bit)

    #print(v_temp)

    cpt = 0
    for i in range(0, taille_chromosome):
        gene = []
        for j in range(0, taille_genes):
            gene.append(v_temp[cpt])
            cpt += 1            
        v_temp_2.append(gene)
        
    #print(v_temp_2)
    
    return v_temp_2

def affichage_pop(une_population):
    cpt = 1
    for chromosome in une_population:
        print('{0:2d}) {1}'.format(cpt, chromosome))
        cpt += 1

def affichage_solution(des_solutions):
    if des_solutions:
        solutions_a_afficher = []
        for une_solution in des_solutions:
            #unpacking du tuple pour comparaison
            [chromosome, num_generation] = une_solution

            temoin_solution = False
            for solution_a_afficher in solutions_a_afficher:
                if affichage_chromosome(chromosome) == solution_a_afficher:
                    temoin_solution = True               
            if temoin_solution == False: #solution pas encore affiche
                solutions_a_afficher.append(affichage_chromosome(chromosome))
                print(' - Generation {0:5d} : {1:6}'.format(num_generation, affichage_chromosome(chromosome)))         
    else:
        print(" - Aucune solution n'a ete trouvee")

def affichage_chromosome(un_chromosome):
    calcul = []
    chaine = ""
    total = 0
    i = 0
    temoin_premier_chiffre = False
        
    #premier chiffre        
    while temoin_premier_chiffre == False and i < len(un_chromosome):
        un_gene = un_chromosome[i]
        for un_gene_nombre in genes_nombres:
            #unpacking du tuple pour comparaison
            [valeur, liste_binaire] = un_gene_nombre
            if un_gene == liste_binaire:
                premier_chiffre = valeur
                temoin_premier_chiffre = True
        if temoin_premier_chiffre == False:
            i += 1
    if temoin_premier_chiffre == True:
        total += premier_chiffre
        i +=1
    else:
        total = 0
    #print("\npremier chiffre :", premier_chiffre)
    #print("i premier chiffre =", i-1)
    calcul.append(str(premier_chiffre))
        
    #Recherche des operations suivantes       
    while i < len(un_chromosome) and temoin_premier_chiffre == True:
        temoin_operateur = False
        temoin_chiffre = False
        operateur = ""
        chiffre = 0
            
        #recherche d'un operateur
        while temoin_operateur == False and i < len(un_chromosome):
            un_gene = un_chromosome[i]
            for un_gene_operateur in genes_operateurs:
                #unpacking du tuple pour comparaison
                [valeur, liste_binaire] = un_gene_operateur
                if un_gene == liste_binaire:
                    operateur = valeur
                    temoin_operateur = True
            i += 1
        #print("operateur :", operateur)
        #print("i operateur =", i-1)
                    
        #recherche d'un chiffre
        while temoin_chiffre == False and i < len(un_chromosome):
            un_gene = un_chromosome[i]
            for un_gene_nombre in genes_nombres:
                #unpacking du tuple pour comparaison
                [valeur, liste_binaire] = un_gene_nombre
                if un_gene == liste_binaire:
                    chiffre = valeur
                    temoin_chiffre = True
            i += 1
        #print("chiffre :", chiffre)
        #print("i chiffre =", i-1)
        if chiffre != 0:
            calcul.append(operateur)
            calcul.append(str(chiffre))
        
        #operation sur le total
        if temoin_chiffre == True and temoin_operateur == True:
            if operateur == "+":
                total += chiffre
            elif operateur == "-":
                total -= chiffre
            elif operateur == "*":
                total *= chiffre
            elif operateur == "/":
                if chiffre == 0: #eviter division par zero
                    total /= 1
                else:
                    total /= chiffre
        #print("total :", total)
    calcul.append("=")
    total = int(total)
    calcul.append(str(total))
    chaine = " ".join(calcul)
    return chaine

#### Main ####

print("=== Algo Genetique 1 ===")

print("\nBienvenue,\n")
print("Il était une fois vivait des créatures appelées sirènes. Les sirènes avaient entièrement évolué dans les confins obscurcis d'une vaste et profonde caverne cachée par la profondeur des entrailles d'une montagne. Elles avaient une vie facile à sentir les pourtours des murs humides de la caverne afin de trouver les algues qui proliféraient entre les rochers et dont elles raffolaient tant. A la saison des amours elles écoutaient avec attention les cris des autres sirènes.")
print("Voici leur histoire :")
while quitter != "N" and quitter != "n":
    population = []
    chromosome = []
    fitness_scores = []
    solutions = []
    generations = input_generations
    cpt_generations = 0
    
    # affichage des variables
    print("\nVariables :\n")
    print("Resultat a trouver :", resultat_a_trouver)
    print("Nombre de generations :", generations)
    print("Taille de la population :", taille_population)
    print("Taille des chromosomes :", taille_chromosome)
    print("Crossover rate :", crossover_rate)
    print("Mutation rate :", mutation_rate)
    print("Seeder pour generer la population de base :", seeder)

    # modif des variables
    choix_modif = input("\n - Voulez-vous modifier ces valeurs? (o,N) : ")
    if choix_modif == "o" or choix_modif == "O" or choix_modif == "y" or choix_modif == "Y":
        choix_modif = "O"
    else:
        choix_modif = "N"

    if choix_modif == "O":
        print("\nModification des variables :\n")

        #Resultat a trouver
        input_resultat_a_trouver = ""
        while input_resultat_a_trouver == "":
            try:
                input_resultat_a_trouver = int(input(" - Resultat a trouver : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre entier")
        resultat_a_trouver = input_resultat_a_trouver

        #Nombre de generations
        input_generations = ""
        while input_generations == "" or int(input_generations) <= 0:
            try:
                input_generations = int(input(" - Nombre de generations (normal : 1000) : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre entier")
        generations = int(input_generations)

        #Taille de la population
        input_taille_population = ""
        while input_taille_population == "" or int(input_taille_population) % 2 or int(input_taille_population) < 2:
            try:
                input_taille_population = int(input(" - Taille de la population (normal : 10, min = 2, chiffre paire obligatoire!) : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre entier")
        taille_population = int(input_taille_population)

        #Taille des chromosomes
        input_taille_chromosome = ""
        while input_taille_chromosome == "" or int(input_taille_chromosome) < 3:
            try:
                input_taille_chromosome = int(input(" - Taille des chromosomes (normal : 9, min = 3) : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre entier")
        taille_chromosome = int(input_taille_chromosome)

        #Crossover rate
        input_crossover_rate = ""
        while input_crossover_rate == "" or float(input_crossover_rate) > 0.9 or float(input_crossover_rate) < 0.1:
            try:
                input_crossover_rate = float(input(" - Crossover rate (normal : 0.7, min = 0.9, max = 0.1) : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre reel")
        crossover_rate = float(input_crossover_rate)

        #Mutation rate
        input_mutation_rate = ""
        while input_mutation_rate == "" or float(input_mutation_rate) > 0.1 or float(input_mutation_rate) <= 0:
            try:
                input_mutation_rate = float(input(" - Mutation rate (normal : 0.001, min > 0, max = 0.1) : "))
            except ValueError:
                print("La valeur saisie est invalide. Le resultat doit etre un nombre reel")
        mutation_rate = float(input_mutation_rate)

        #Seeder   
        input_seeder = ""
        while input_seeder == "":
            input_seeder = input(" - Seeder pour generer la population de base : ")
        seeder = input_seeder
            
    # boucle n generations

    # 1) Creation d'une population
    random.seed(seeder)
    cpt_generations += 1
    population = creation_population(taille_population)
    print("\nPopulation de depart :\n")
    affichage_pop(population)

    print("\nVeuillez patienter ...\n")
    # 2) Calcul du fitness score

    fitness_scores = calcul_fitness_scores(population)
    #print("\nFitness scores :\n\n", fitness_scores)

    # 3) Roulette wheel

    indice_chromosome_1 = roulette_wheel()
    indice_chromosome_2 = roulette_wheel()
    while indice_chromosome_1 == indice_chromosome_2 :
        indice_chromosome_2 = roulette_wheel() 
    #print("\nIndices des chromosomes a reproduire :\n\n", indice_chromosome_1, "et", indice_chromosome_2)

    # 4) Reproduction

    #print("\nReproduction:\n")
    nouvelle_population = []
    cpt_pop = taille_population/2
    while cpt_pop:
        for nouveau_chromosome in reproduction(indice_chromosome_1, indice_chromosome_2):
            nouvelle_population.append(nouveau_chromosome)
        cpt_pop -= 1
        
    #print(":\n - Generation", cpt_generations, ":\n")
    #affichage_pop(nouvelle_population)

    while generations-1 :
        cpt_generations += 1
        #operations a repeter
        fitness_scores = []
        fitness_scores = calcul_fitness_scores(nouvelle_population)
        indice_chromosome_1 = roulette_wheel()
        indice_chromosome_2 = roulette_wheel()
        while indice_chromosome_1 == indice_chromosome_2 :
            indice_chromosome_2 = roulette_wheel()
        nouvelle_population = []
        cpt_pop = taille_population/2
        while cpt_pop:
            for nouveau_chromosome in reproduction(indice_chromosome_1, indice_chromosome_2):
                nouvelle_population.append(nouveau_chromosome)
            cpt_pop -= 1
        generations -= 1
        #print(":\n - Generation", cpt_generations, ":\n")
        #affichage_pop(nouvelle_population)

    # 5) Affichage des solutions
    print("Solutions differentes trouvees :\n")
    affichage_solution(solutions)

    quitter = input("\nRelancer l'algorithme genetique? (O/n)\n")
